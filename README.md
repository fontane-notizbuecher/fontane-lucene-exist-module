# About

A xar module containing an lucene analyzer for usage with the [eXist](http://exist-db.org/) lucene index. 
It chains the UmlautFilter from [fwb](https://github.com/subugoe/fwb-solr-mods) and the SolrSynonymParser in the following order:

1. apply character mappings (e.g. "ae" for "ä") 
2. apply synonyms 


# Build

This projects uses [maven](https://maven.apache.org/) to build.

Command:

        mvn clean package

Find the xar for installation in the eXist-DB in `target/fontane-lucene-exist-module-VERSION.xar`.

# Install

Install fontane-lucene-exist-module-VERSION.xar with the eXist package manager from the dashboard. It may be necessary to restart eXist afterwards to make sure to have the module available for eXist on the Java class path.

# Configure

The analyzer is configured with the [eXist index configuration](http://exist-db.org/exist/apps/doc/indexing#D3.15.19) inside the eXist database.
It points to files on the hard disk for synonyms or the character mappings.

Example:

        <collection xmlns="http://exist-db.org/collection-config/1.0">
            <index xmlns:tei="http://www.tei-c.org/ns/1.0">
              <lucene>
                  <analyzer id="fontane" class="eu.dariah.de.fontane.lucene.FontaneAnalyzer">
                    <param name="mappings" type="java.io.FileReader" value="/tmp/umlaut_mappings.txt"/>
                    <param name="synonyms" type="java.io.FileReader" value="/tmp/synonyms.txt"/>
                  </analyzer>
                  <text qname="tei:surface" analyzer="fontane">
                    <inline qname="tei:seg"/>
                  </text>
              </lucene>
            </index>
        </collection>


## Synonyms

Point to a file written in the Solr synonym syntax:

https://lucene.apache.org/core/4_0_0/analyzers-common/org/apache/lucene/analysis/synonym/SolrSynonymParser.html


## Mappings

Point to a file with character mappings. Uses the UmlautFilterFactory: https://github.com/subugoe/fwb-solr-mods#documentation
    

