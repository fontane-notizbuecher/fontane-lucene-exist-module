package eu.dariah.de.fontane.lucene;

import java.io.IOException;
import java.io.Reader;
import java.text.ParseException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.synonym.SolrSynonymParser;
import org.apache.lucene.analysis.synonym.SynonymFilter;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.WordlistLoader;
import org.apache.lucene.util.Version;

import sub.fwb.UmlautFilter;

public class FontaneAnalyzer extends Analyzer {
	
	private Version matchVersion = Version.LUCENE_4_10_4;
	private static SynonymMap synonyms;
	private CharArraySet mappings;
	
	// the analyzer could not be found in xar, if this constructor not available
	public FontaneAnalyzer(Version matchVersion, Reader mappings, Reader synonyms) {
		//super();
		this(mappings, synonyms);
	}
	
	public FontaneAnalyzer(Reader mappings, Reader synonyms) {
		
		Analyzer analyzer = new StandardAnalyzer();
		SolrSynonymParser parser = new SolrSynonymParser(true, false, analyzer);
		try {
			parser.parse(synonyms);
			this.synonyms = parser.build(); 
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        try {
			this.mappings = WordlistLoader.getWordSet(mappings);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected TokenStreamComponents createComponents(String fieldName, Reader reader) {

	    Tokenizer source = new StandardTokenizer(reader);
	    TokenStream result = new LowerCaseFilter(source);
	    result = new UmlautFilter(result, mappings);
	    result = new SynonymFilter(result, synonyms, false);
	    
	    return new TokenStreamComponents(source, result);

	}

}
